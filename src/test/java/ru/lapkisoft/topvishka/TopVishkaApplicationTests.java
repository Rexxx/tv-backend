package ru.lapkisoft.topvishka;

import okhttp3.Request;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.lapkisoft.topvishka.connector.HttpClient;
import ru.lapkisoft.topvishka.dao.repository.EducationalProgramDaoImpl;
import ru.lapkisoft.topvishka.dao.repository.TeacherDao;
import ru.lapkisoft.topvishka.dao.repository.TeacherDaoImpl;
import ru.lapkisoft.topvishka.model.Teacher;
import ru.lapkisoft.topvishka.model.VikonEdProgram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TopVishkaApplicationTests {
	private static TeacherDao teacherDao;

	@BeforeClass
	public static void setup() {
		teacherDao = new TeacherDaoImpl();
	}

	@AfterClass
	public static void cleanUp() {
		teacherDao = null;
	}

	@Test
	public void test() throws IOException {

	}
}
