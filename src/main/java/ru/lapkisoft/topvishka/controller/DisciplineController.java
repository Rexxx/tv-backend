package ru.lapkisoft.topvishka.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.lapkisoft.topvishka.exception.BadRequestException;
import ru.lapkisoft.topvishka.model.Discipline;
import ru.lapkisoft.topvishka.service.api.DisciplineService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/disciplines")
public class DisciplineController {
    private final DisciplineService disciplineService;

    public DisciplineController(DisciplineService disciplineService) {
        this.disciplineService = disciplineService;
    }

    @PostMapping
    public Discipline insert(@Valid @RequestBody Discipline discipline) {
        return disciplineService.insert(discipline);
    }

    @GetMapping("{id}")
    public Discipline get(@PathVariable("id") UUID id) {
        try {
            return disciplineService.findById(id);
        }
        catch (IllegalArgumentException ex) {
            throw new BadRequestException(ex.getMessage());
        }
    }

    @GetMapping
    public List<Discipline> getAll() {
        return disciplineService.getAll();
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") UUID id) {
        disciplineService.delete(id);
    }

    @GetMapping("by_ed_program/{id}")
    public List<Discipline> getAllByEdProgramId(@PathVariable("id") UUID id) {
        return disciplineService.getAllByEdProgramId(id);
    }
}
