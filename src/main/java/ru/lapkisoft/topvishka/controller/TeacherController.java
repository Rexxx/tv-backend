package ru.lapkisoft.topvishka.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.lapkisoft.topvishka.exception.BadRequestException;
import ru.lapkisoft.topvishka.model.Teacher;
import ru.lapkisoft.topvishka.model.VikonEdProgram;
import ru.lapkisoft.topvishka.model.VikonTeacher;
import ru.lapkisoft.topvishka.service.api.TeacherService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/teachers")
public class TeacherController {
    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping
    public Teacher insert(@Valid @RequestBody Teacher teacher) {
        return teacherService.insert(teacher);
    }

    @GetMapping("{id}")
    public Teacher get(@PathVariable("id") UUID id) {
        try {
            return teacherService.findById(id);
        }
        catch (IllegalArgumentException ex) {
            throw new BadRequestException(ex.getMessage());
        }
    }

    @GetMapping
    public List<Teacher> getAll() {
        return teacherService.getAll();
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") UUID id) {
        teacherService.delete(id);
    }

    @GetMapping("vikon/parse")
    public List<VikonTeacher> parseVikon(@RequestParam String url) {
        return teacherService.parseVikon(url);
    }
}
