package ru.lapkisoft.topvishka.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.lapkisoft.topvishka.exception.BadRequestException;
import ru.lapkisoft.topvishka.model.EducationalProgram;
import ru.lapkisoft.topvishka.model.EducationalProgramCreationInfo;
import ru.lapkisoft.topvishka.model.VikonEdProgram;
import ru.lapkisoft.topvishka.service.api.EducationalProgramService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/ed-programs")
public class EducationalProgramController {
    private final EducationalProgramService educationalProgramService;

    public EducationalProgramController(EducationalProgramService educationalProgramService) {
        this.educationalProgramService = educationalProgramService;
    }

    @PostMapping
    public EducationalProgram insert(@Valid @RequestBody EducationalProgramCreationInfo creationInfo) {
        return educationalProgramService.insert(creationInfo);
    }

    @GetMapping("{id}")
    public EducationalProgram get(@PathVariable("id") UUID id) {
        try {
            return educationalProgramService.findById(id);
        }
        catch (IllegalArgumentException ex) {
            throw new BadRequestException(ex.getMessage());
        }
    }

    @GetMapping
    public List<EducationalProgram> getAll() {
        return educationalProgramService.getAll();
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") UUID id) {
        educationalProgramService.delete(id);
    }

    @PostMapping("{id}/add_keyword")
    public EducationalProgram addVacanciesKeyword(@PathVariable("id") UUID id, @RequestParam String keyword) {
        return educationalProgramService.addVacanciesKeyword(id, keyword);
    }

    @PostMapping("{id}/remove_keyword")
    public EducationalProgram removeVacanciesKeyword(@PathVariable("id") UUID id, @RequestParam String keyword) {
        return educationalProgramService.removeVacanciesKeyword(id, keyword);
    }

    @PostMapping("{id}/collect_info")
    public EducationalProgram collectInfo(@PathVariable("id") UUID id) {
        return educationalProgramService.collectInfo(id);
    }

    @GetMapping("vikon/parse")
    public List<VikonEdProgram> parseVikon(@RequestParam String url) {
        return educationalProgramService.parseVikon(url);
    }
}
