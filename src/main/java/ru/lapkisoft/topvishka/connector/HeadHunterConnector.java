package ru.lapkisoft.topvishka.connector;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.lapkisoft.topvishka.model.VacanciesInfo;

import java.io.IOException;
import java.util.List;

public class HeadHunterConnector {
    private static final String URL = "https://api.hh.ru/vacancies";
    private static final String USER_AGENT_FIELD_NAME = "User-Agent";
    private static final String USER_AGENT_VALUE = "api-test-agent";
    private static final String AREA_RUSSIA_CODE = "113";
    private static final String AREA_ROSTOV_CODE = "1530";
    private static final String WHITESPACE = "%20";
    private static final int AVG_ROSTOV_WAGE = 32006;

    public static VacanciesInfo getVacanciesInfo(List<String> keywords) throws IOException {
        VacanciesInfo info = new VacanciesInfo();

        for (String keyword : keywords) {
            keyword = keyword.replace(" ", WHITESPACE);

            String cityVacanciesJson = sendRequest(URL_ROSTOV + keyword);
            int cityVacanciesCount = getVacanciesCount(cityVacanciesJson);
            info.increaseVacanciesCountInCity(cityVacanciesCount);
            int avgWage = getAverageWage(cityVacanciesJson);
            info.considerAverageWage(avgWage);
            info.considerWageRatio(avgWage / (AVG_ROSTOV_WAGE * 1.));

            String countryVacanciesJson = sendRequest(URL_RUSSIA + keyword);
            int countryVacanciesCount = getVacanciesCount(countryVacanciesJson);
            info.increaseVacanciesCountInCountry(countryVacanciesCount);
        }

        return info;
    }

    private static final String URL_ROSTOV = URL + "?area=" + AREA_ROSTOV_CODE + "&text=";
    private static final String URL_RUSSIA = URL + "?area=" + AREA_RUSSIA_CODE + "&text=";

    private static String sendRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .header(USER_AGENT_FIELD_NAME, USER_AGENT_VALUE)
                .build();

        Response response = HttpClient.client.newCall(request).execute();
        return response.body().string();
    }

    private static final String FOUND_FIELD = "found";

    private static int getVacanciesCount(String json) {
        JSONObject obj = new JSONObject(json);
        return obj.getInt(FOUND_FIELD);
    }

    private static final String ITEMS_FIELD = "items";
    private static final String SALARY_FIELD = "salary";
    private static final String FROM_FIELD = "from";
    private static final String TO_FIELD = "to";

    private static int getAverageWage(String json) {
        JSONObject obj = new JSONObject(json);
        JSONArray arr = obj.getJSONArray(ITEMS_FIELD);
        int sum = 0, counter = 0;

        for (int i = 0; i < arr.length(); i++) {
            JSONObject item = arr.getJSONObject(i);

            if (item.isNull(SALARY_FIELD)) {
                continue;
            }

            JSONObject salary = item.getJSONObject(SALARY_FIELD);
            int wage;

            if (salary.isNull(FROM_FIELD)) {
                wage = arr.getJSONObject(i).getJSONObject(SALARY_FIELD).getInt(TO_FIELD);
            }
            else {
                wage = arr.getJSONObject(i).getJSONObject(SALARY_FIELD).getInt(FROM_FIELD);
            }

            sum += wage;
            counter++;
        }

        return counter == 0 ? counter : sum / counter;
    }
}
