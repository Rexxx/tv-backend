package ru.lapkisoft.topvishka.connector;

import okhttp3.Request;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.lapkisoft.topvishka.model.VikonEdProgram;
import ru.lapkisoft.topvishka.model.VikonTeacher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VikonConnector {
    private static final String HTTP = "https://";
    private static final String ED_PROGRAM_URL = "/sveden/education/eduOp/";
    private static final String TEACHER_URL = "/sveden/employees/pps/";

    public static List<VikonEdProgram> getEdPrograms(String url) throws IOException {
        String html = sendRequest(HTTP + url + ED_PROGRAM_URL);
        return parseEdPrograms(html, HTTP + url);
    }

    public static List<VikonTeacher> getTeachers(String url) throws IOException {
        String html = sendRequest(HTTP + url + TEACHER_URL);
        return parseTeachers(html, HTTP + url);
    }

    private static String sendRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = HttpClient.client.newCall(request).execute();
        return response.body().string();
    }

    private static final String ITEMPROP_FIELD = "itemprop";
    private static final String ITEMPROP_EDU_OP_VALUE = "eduOp";
    private static final String ITEMPROP_CODE_VALUE = "eduCode";
    private static final String ITEMPROP_LEVEL_VALUE = "eduLevel";
    private static final String ITEMPROP_OP_NAME_VALUE = "eduName";
    private static final String ITEMPROP_ANNOTATION_VALUE = "educationAnnotation";
    private static final String TAG_A = "a";
    private static final String ATTR_HREF = "href";

    private static List<VikonEdProgram> parseEdPrograms(String content, String url) {
        Document html = Jsoup.parse(content);
        Elements eduOps = html.body().getElementsByAttributeValue(ITEMPROP_FIELD, ITEMPROP_EDU_OP_VALUE);
        List<VikonEdProgram> programs = new ArrayList<>();

        for (Element eduOp : eduOps) {
            Elements eduCode = eduOp.getElementsByAttributeValue(ITEMPROP_FIELD, ITEMPROP_CODE_VALUE);

            if (eduCode.size() != 1 || eduCode.get(0).getElementsByTag(TAG_A).size() != 1) {
                continue;
            }

            String code = eduCode.get(0).getElementsByTag(TAG_A).get(0).text();
            Elements eduLevels = eduOp.getElementsByAttributeValue(ITEMPROP_FIELD, ITEMPROP_LEVEL_VALUE);
            Elements eduNames = eduOp.getElementsByAttributeValue(ITEMPROP_FIELD, ITEMPROP_OP_NAME_VALUE);
            Elements eduAnnotations = eduOp.getElementsByAttributeValue(ITEMPROP_FIELD, ITEMPROP_ANNOTATION_VALUE);

            if (eduLevels.size() != eduNames.size() && eduLevels.size() != eduAnnotations.size()) {
                continue;
            }

            for (int i = 0; i < eduNames.size(); i++) {
                VikonEdProgram program = new VikonEdProgram();
                program.setCode(code);
                program.setLevel(eduLevels.get(i).text());
                program.setName(eduNames.get(i).child(0).text());

                if (eduAnnotations.get(i).getElementsByTag(TAG_A).size() != 0) {
                    program.setSyllabusLink(url + eduAnnotations.get(i).getElementsByTag(TAG_A).get(0).attr(ATTR_HREF));
                }

                programs.add(program);
            }
        }

        return programs;
    }

    private static final String ITEMPROP_TEACHER_NAME_VALUE = "fio";
    private static final String ITEMPROP_POSITION_VALUE = "post";
    private static final String ITEMPROP_DISCIPLINES_VALUE = "teachingDiscipline";

    private static List<VikonTeacher> parseTeachers(String content, String url) {
        Document html = Jsoup.parse(content);
        List<VikonTeacher> teachers = new ArrayList<>();
        Elements names = html.body().getElementsByAttributeValue(ITEMPROP_FIELD, ITEMPROP_TEACHER_NAME_VALUE);
        Elements positions = html.body().getElementsByAttributeValue(ITEMPROP_FIELD, ITEMPROP_POSITION_VALUE);
        Elements disciplines = html.body().getElementsByAttributeValue(ITEMPROP_FIELD, ITEMPROP_DISCIPLINES_VALUE);

        for (int i = 0; i < names.size(); i++) {
            VikonTeacher teacher = new VikonTeacher();
            Elements namesLinks = names.get(i).getElementsByTag(TAG_A);

            if (namesLinks.size() == 0) {
                teacher.setName(names.get(i).text());
            }
            else {
                teacher.setName(names.get(i).getElementsByTag(TAG_A).get(0).text());
            }

            teacher.setPosition(positions.get(i).text());
            teacher.setDisciplines(prepareDisciplines(disciplines.get(i).text()));
            teachers.add(teacher);
        }

        return teachers;
    }

    private static final String LINK_GARBAGE = "показать все";

    private static final List<String> prepareDisciplines(String rawDisciplines) {
        String[] disciplines = rawDisciplines.split("(?=\\p{Lu})");

        for (int i = 0; i < disciplines.length; i++) {
            disciplines[i] = disciplines[i].replace(LINK_GARBAGE, "");
            disciplines[i] = disciplines[i].trim();
            disciplines[i] = disciplines[i].replaceAll("[;,.]", "");
        }

        return Arrays.asList(disciplines);
    }
}
