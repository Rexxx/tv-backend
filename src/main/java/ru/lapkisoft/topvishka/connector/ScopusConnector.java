package ru.lapkisoft.topvishka.connector;

import okhttp3.Request;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class ScopusConnector {
    public static int getHirshIndex(String url) throws IOException {
        String html = sendRequest(url);
        return parseHirshIndex(html);
    }

    private static String sendRequest(String url) {
        Request request = new Request.Builder()
                .url(url)
                //todo небольшой такой костылик)))
//                .header("Cookie", "__cfduid=d36e187a1ea9745ba6f6584a37711788b1592700060; SCSessionID=B5CD86F7661C2CDC16C15D689F0CFFA6.wsnAw8kcdt7IPYLO0V48gA; scopusSessionUUID=c80e17fc-7391-4445-8; scopus.machineID=B5CD86F7661C2CDC16C15D689F0CFFA6.wsnAw8kcdt7IPYLO0V48gA; AWSELB=CB9317D502BF07938DE10C841E762B7A33C19AADB1A92D1850FFD478AD2623F041C6907A4857150409AA2A6B45282987E001D9FF3F2CFBB76ECCEBE0946FCCE2B9E27272A3B908F01E3F2ECCB40C1E0FEE67FB4C8D; __cfruid=67a390bb6371c6a698cdccfc04ee17cab0bda212-1592700061")
                .build();

        try {
            Response response = HttpClient.client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            return "";  // скопус опасный, редко может вызвать too many follow-up requests
        }
    }

    private static int parseHirshIndex(String content) {
        Document html = Jsoup.parse(content);
        Element section = html.body().getElementById("authorDetailsHindex");

        if (section == null) {
            return 0;
        }

        Elements index = section.getElementsByClass("fontLarge");

        if (index.size() != 1) {
            return 0;
        }

        return Integer.parseInt(index.get(0).text());
    }
}
