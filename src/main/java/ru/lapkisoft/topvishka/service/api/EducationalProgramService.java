package ru.lapkisoft.topvishka.service.api;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.lapkisoft.topvishka.connector.HeadHunterConnector;
import ru.lapkisoft.topvishka.connector.ScopusConnector;
import ru.lapkisoft.topvishka.connector.VikonConnector;
import ru.lapkisoft.topvishka.dao.repository.DisciplineDao;
import ru.lapkisoft.topvishka.dao.repository.EducationalProgramDao;
import ru.lapkisoft.topvishka.dao.repository.TeacherDao;
import ru.lapkisoft.topvishka.exception.NotFoundException;
import ru.lapkisoft.topvishka.exception.TeapotException;
import ru.lapkisoft.topvishka.model.EducationalProgram;
import ru.lapkisoft.topvishka.model.EducationalProgramCreationInfo;
import ru.lapkisoft.topvishka.model.EducationalProgramExternalInfo;
import ru.lapkisoft.topvishka.model.Teacher;
import ru.lapkisoft.topvishka.model.VacanciesInfo;
import ru.lapkisoft.topvishka.model.VikonEdProgram;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class EducationalProgramService {
    private final EducationalProgramDao educationalProgramDao;
    private final TeacherDao teacherDao;

    public EducationalProgram insert(EducationalProgramCreationInfo creationInfo) {
        EducationalProgram edProgram = new EducationalProgram();
        edProgram.setId(UUID.randomUUID());
        edProgram.setName(creationInfo.getName());
        edProgram.setDescription(creationInfo.getDescription());
        edProgram.setSyllabusLink(creationInfo.getSyllabusLink());
        LocalDateTime now = LocalDateTime.now();
        edProgram.setCreatedDate(now);
        edProgram.setUpdatedDate(now);
        educationalProgramDao.insert(edProgram);
        edProgram.setVacanciesKeywords(new ArrayList<>());
        edProgram.setRate(0.);
        edProgram.setVacanciesCountInCity(0);
        edProgram.setVacanciesCountInCountry(0);
        edProgram.setAverageWage(0.);
        edProgram.setWageRatio(0.);
        edProgram.setAverageHirshIndex(0);
        edProgram.setPractitionerPortion(0.);
        edProgram.setOnlineCoursesCount(0);
        edProgram.setAverageStudentEstimation(0);

        return edProgram;
    }

    public EducationalProgram findById(UUID id) {
        EducationalProgram edProgram = educationalProgramDao.findById(id);

        if (edProgram == null) {
            throw new NotFoundException("EducationalProgram with specified id not found");
        }

        return edProgram;
    }

    public List<EducationalProgram> getAll() {
        return educationalProgramDao.getAll();
    }

    public void delete(UUID id) {
        educationalProgramDao.delete(id);
    }

    public EducationalProgram addVacanciesKeyword(UUID id, String keyword) {
        educationalProgramDao.addVacanciesKeyword(id, keyword);
        return educationalProgramDao.findById(id);
    }

    public EducationalProgram removeVacanciesKeyword(UUID id, String keyword) {
        educationalProgramDao.removeVacanciesKeyword(id, keyword);
        return educationalProgramDao.findById(id);
    }

    public EducationalProgram collectInfo(UUID id) {
        EducationalProgram edProgram = educationalProgramDao.findById(id);

        if (edProgram == null) {
            throw new NotFoundException("EducationalProgram with specified id not found");
        }

        try {
            VacanciesInfo vacanciesInfo = HeadHunterConnector.getVacanciesInfo(edProgram.getVacanciesKeywords());
            List<Teacher> teachers = teacherDao.getTeachersByEdProgramId(id);
            int hirshIndex = processHirshIndexes(teachers);
            double rate = computeRate(vacanciesInfo, hirshIndex);

            EducationalProgramExternalInfo externalInfo = new EducationalProgramExternalInfo(rate, vacanciesInfo, hirshIndex);
            educationalProgramDao.updateExternalInfo(id, externalInfo);

            return educationalProgramDao.findById(id);
        }
        catch (IOException ex) {
            throw new TeapotException("Service error. Make a tea and call administrator");
        }
    }

    private int processHirshIndexes(List<Teacher> teachers) throws IOException {
        int sum = 0;

        for (Teacher teacher : teachers) {
            int hirshIndex = ScopusConnector.getHirshIndex(teacher.getScopusLink());
            teacher.setHirshIndex(hirshIndex);
            teacherDao.updateHirshIndex(teacher.getId(), hirshIndex);
            sum += hirshIndex;
        }

        return teachers.size() == 0 ? 0 : sum / teachers.size();
    }

    private final double STANDARD_CITY_VACANCIES_COUNT = 500;
    private final double STANDARD_COUNTRY_VACANCIES_COUNT = 25000;
    private final int HIRSH_INDEX_MIN_VALUE = 2;
    private final int RATE_COEFF = 10;

    private double computeRate(VacanciesInfo info, int hirshIndex) {
        double rate = 0;
        rate += Math.min((info.getVacanciesCountInCity() / STANDARD_CITY_VACANCIES_COUNT) * RATE_COEFF, RATE_COEFF);
        rate += Math.min((info.getVacanciesCountInCountry() / STANDARD_COUNTRY_VACANCIES_COUNT) * RATE_COEFF, RATE_COEFF);
        rate += Math.min(info.getWageRatio() * RATE_COEFF / 2, RATE_COEFF);
        rate += hirshIndex < HIRSH_INDEX_MIN_VALUE ? 0 : Math.min(hirshIndex + 3, RATE_COEFF);

        return rate / 4;    // делим на кол-во показателей, которых тут пока не все
    }

    public List<VikonEdProgram> parseVikon(String url) {
        try {
            return VikonConnector.getEdPrograms(url);
        } catch (IOException e) {
            throw new TeapotException("Service error. Make a tea and call administrator");
        }
    }
}
