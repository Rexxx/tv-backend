package ru.lapkisoft.topvishka.service.api;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.lapkisoft.topvishka.connector.VikonConnector;
import ru.lapkisoft.topvishka.dao.repository.TeacherDao;
import ru.lapkisoft.topvishka.exception.NotFoundException;
import ru.lapkisoft.topvishka.exception.TeapotException;
import ru.lapkisoft.topvishka.model.Teacher;
import ru.lapkisoft.topvishka.model.VikonEdProgram;
import ru.lapkisoft.topvishka.model.VikonTeacher;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class TeacherService {
    private final TeacherDao teacherDao;

    public Teacher insert(Teacher teacher) {
        teacher.setId(UUID.randomUUID());
        teacher.setHirshIndex(0);
        LocalDateTime now = LocalDateTime.now();
        teacher.setCreatedDate(now);
        teacher.setUpdatedDate(now);
        teacherDao.insert(teacher);

        return teacher;
    }

    public Teacher findById(UUID id) {
        Teacher teacher = teacherDao.findById(id);

        if (teacher == null) {
            throw new NotFoundException("Teacher with specified id not found");
        }

        return teacher;
    }

    public List<Teacher> getAll() {
        return teacherDao.getAll();
    }

    public void delete(UUID id) {
        teacherDao.delete(id);
    }

    public List<VikonTeacher> parseVikon(String url) {
        try {
            return VikonConnector.getTeachers(url);
        } catch (IOException e) {
            throw new TeapotException("Service error. Make a tea and call administrator");
        }
    }
}
