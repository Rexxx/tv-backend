package ru.lapkisoft.topvishka.service.api;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.lapkisoft.topvishka.dao.repository.DisciplineDao;
import ru.lapkisoft.topvishka.exception.NotFoundException;
import ru.lapkisoft.topvishka.model.Discipline;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class DisciplineService {
    private final DisciplineDao disciplineDao;

    public Discipline insert(Discipline discipline) {
        discipline.setId(UUID.randomUUID());
        LocalDateTime now = LocalDateTime.now();
        discipline.setCreatedDate(now);
        discipline.setUpdatedDate(now);
        disciplineDao.insert(discipline);

        return discipline;
    }

    public Discipline findById(UUID id) {
        Discipline discipline = disciplineDao.findById(id);

        if (discipline == null) {
            throw new NotFoundException("Discipline with specified id not found");
        }

        return discipline;
    }

    public List<Discipline> getAll() {
        return disciplineDao.getAll();
    }

    public void delete(UUID id) {
        disciplineDao.delete(id);
    }

    public List<Discipline> getAllByEdProgramId(UUID id) {
        return disciplineDao.getAllByEdProgramId(id);
    }
}
