package ru.lapkisoft.topvishka.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EducationalProgramExternalInfo {
    private double rate;
    private VacanciesInfo vacanciesInfo;
    private int hirshIndex;
}
