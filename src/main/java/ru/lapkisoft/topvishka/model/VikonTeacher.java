package ru.lapkisoft.topvishka.model;

import lombok.Data;

import java.util.List;

@Data
public class VikonTeacher {
    private String name;
    private String position;
    private List<String> disciplines;
}
