package ru.lapkisoft.topvishka.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Teacher extends PojoModel {
    private String name;
    private String scopusLink;
    private int hirshIndex;
}
