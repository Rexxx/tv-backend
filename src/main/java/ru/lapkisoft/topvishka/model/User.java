package ru.lapkisoft.topvishka.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class User extends PojoModel {
    private String name;
    private String email;
    private String role;
    private String password;
}
