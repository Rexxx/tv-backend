package ru.lapkisoft.topvishka.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class EducationalProgramCreationInfo {
    @NotBlank
    private String name;
    @NotBlank
    private String description;
    @NotBlank
    private String syllabusLink;
}
