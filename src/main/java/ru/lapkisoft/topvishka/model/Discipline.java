package ru.lapkisoft.topvishka.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
public class Discipline extends PojoModel {
    private String name;
    private UUID teacherId;
    private UUID edProgramId;
}
