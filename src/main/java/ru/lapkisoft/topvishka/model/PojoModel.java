package ru.lapkisoft.topvishka.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class PojoModel {
    private UUID id;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;
}
