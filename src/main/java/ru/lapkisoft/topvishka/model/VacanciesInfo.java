package ru.lapkisoft.topvishka.model;

import lombok.Data;

@Data
public class VacanciesInfo {
    private int vacanciesCountInCity;
    private int vacanciesCountInCountry;
    private double averageWage;
    private double wageRatio;

    public void increaseVacanciesCountInCity(int vacanciesCountInCity) {
        this.vacanciesCountInCity += vacanciesCountInCity;
    }

    public void increaseVacanciesCountInCountry(int vacanciesCountInCountry) {
        this.vacanciesCountInCountry += vacanciesCountInCountry;
    }

    public void considerAverageWage(double averageWage) {
        this.averageWage += averageWage;
        this.averageWage /= 2;
    }

    public void considerWageRatio(double wageRatio) {
        this.wageRatio += wageRatio;
        this.wageRatio /= 2;
    }
}
