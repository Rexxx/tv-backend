package ru.lapkisoft.topvishka.model;

import lombok.Data;

/** По сути тоже самое, что и EducationalProgram, просто как фича, чтобы уже до защиты ничего не сломать */
@Data
public class VikonEdProgram {
    private String code;
    private String name;
    private String level;
    private String syllabusLink;
}
