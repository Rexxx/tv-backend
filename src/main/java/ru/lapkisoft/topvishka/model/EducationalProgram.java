package ru.lapkisoft.topvishka.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class EducationalProgram extends PojoModel {
    private String name;
    private Double rate;
    private String description;
    private String syllabusLink;
    private List<String> vacanciesKeywords;
    private Integer vacanciesCountInCity;
    private Integer vacanciesCountInCountry;
    private Double averageWage;
    private Double wageRatio;
    private Integer averageHirshIndex;
    private Double practitionerPortion;
    private Integer onlineCoursesCount;
    private Integer averageStudentEstimation;
}
