package ru.lapkisoft.topvishka.dao.repository;

import org.apache.ibatis.session.SqlSession;
import ru.lapkisoft.topvishka.dao.mapper.UserMapper;
import ru.lapkisoft.topvishka.model.User;
import ru.lapkisoft.topvishka.service.SessionFactoryService;

import java.util.UUID;

public class UserRepository {
    public void insert(User item) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);
            mapper.insert(item);
            session.commit();
        }
    }

    public User findById(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);
            return mapper.findById(id);
        }
    }

    public void delete(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);
            mapper.delete(id);
            session.commit();
        }
    }

    public User findByEmail(String email) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);
            return mapper.findByEmail(email);
        }
    }
}
