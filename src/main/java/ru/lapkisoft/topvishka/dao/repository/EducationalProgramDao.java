package ru.lapkisoft.topvishka.dao.repository;

import org.springframework.stereotype.Repository;
import ru.lapkisoft.topvishka.model.EducationalProgram;
import ru.lapkisoft.topvishka.model.EducationalProgramExternalInfo;

import java.util.List;
import java.util.UUID;

@Repository
public interface EducationalProgramDao {
    void insert(EducationalProgram item);
    EducationalProgram findById(UUID id);
    List<EducationalProgram> getAll();
    void delete(UUID id);
    void addVacanciesKeyword(UUID id, String keyword);
    void removeVacanciesKeyword(UUID id, String keyword);
    void updateExternalInfo(UUID id, EducationalProgramExternalInfo info);
}
