package ru.lapkisoft.topvishka.dao.repository;

import org.springframework.stereotype.Repository;
import ru.lapkisoft.topvishka.model.Discipline;

import java.util.List;
import java.util.UUID;

@Repository
public interface DisciplineDao {
    void insert(Discipline item);
    Discipline findById(UUID id);
    List<Discipline> getAll();
    void delete(UUID id);
    List<Discipline> getAllByEdProgramId(UUID id);
}
