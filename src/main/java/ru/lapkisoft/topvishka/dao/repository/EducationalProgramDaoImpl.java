package ru.lapkisoft.topvishka.dao.repository;

import org.apache.ibatis.session.SqlSession;
import ru.lapkisoft.topvishka.dao.mapper.EducationalProgramMapper;
import ru.lapkisoft.topvishka.model.EducationalProgram;
import ru.lapkisoft.topvishka.model.EducationalProgramExternalInfo;
import ru.lapkisoft.topvishka.service.SessionFactoryService;

import java.util.List;
import java.util.UUID;

public class EducationalProgramDaoImpl implements EducationalProgramDao {
    public void insert(EducationalProgram item) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            EducationalProgramMapper mapper = session.getMapper(EducationalProgramMapper.class);
            mapper.insert(item);
            session.commit();
        }
    }

    public EducationalProgram findById(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            EducationalProgramMapper mapper = session.getMapper(EducationalProgramMapper.class);
            return mapper.findById(id);
        }
    }

    @Override
    public List<EducationalProgram> getAll() {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            EducationalProgramMapper mapper = session.getMapper(EducationalProgramMapper.class);
            return mapper.getAll();
        }
    }

    public void delete(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            EducationalProgramMapper mapper = session.getMapper(EducationalProgramMapper.class);
            mapper.delete(id);
            session.commit();
        }
    }

    @Override
    public void addVacanciesKeyword(UUID id, String keyword) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            EducationalProgramMapper mapper = session.getMapper(EducationalProgramMapper.class);
            mapper.addVacanciesKeyword(id, keyword);
            session.commit();
        }
    }

    @Override
    public void removeVacanciesKeyword(UUID id, String keyword) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            EducationalProgramMapper mapper = session.getMapper(EducationalProgramMapper.class);
            mapper.removeVacanciesKeyword(id, keyword);
            session.commit();
        }
    }

    @Override
    public void updateExternalInfo(UUID id, EducationalProgramExternalInfo info) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            EducationalProgramMapper mapper = session.getMapper(EducationalProgramMapper.class);
            mapper.updateExternalInfo(id, info);
            session.commit();
        }
    }
}
