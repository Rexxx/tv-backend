package ru.lapkisoft.topvishka.dao.repository;

import org.apache.ibatis.session.SqlSession;
import ru.lapkisoft.topvishka.dao.mapper.DisciplineMapper;
import ru.lapkisoft.topvishka.model.Discipline;
import ru.lapkisoft.topvishka.service.SessionFactoryService;

import java.util.List;
import java.util.UUID;

public class DisciplineDaoImpl implements DisciplineDao {
    public void insert(Discipline item) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            DisciplineMapper mapper = session.getMapper(DisciplineMapper.class);
            mapper.insert(item);
            session.commit();
        }
    }

    public Discipline findById(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            DisciplineMapper mapper = session.getMapper(DisciplineMapper.class);
            return mapper.findById(id);
        }
    }

    @Override
    public List<Discipline> getAll() {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            DisciplineMapper mapper = session.getMapper(DisciplineMapper.class);
            return mapper.getAll();
        }
    }

    public void delete(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            DisciplineMapper mapper = session.getMapper(DisciplineMapper.class);
            mapper.delete(id);
            session.commit();
        }
    }

    @Override
    public List<Discipline> getAllByEdProgramId(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            DisciplineMapper mapper = session.getMapper(DisciplineMapper.class);
            return mapper.getAllByEdProgramId(id);
        }
    }
}
