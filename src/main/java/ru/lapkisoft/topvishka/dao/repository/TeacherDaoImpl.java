package ru.lapkisoft.topvishka.dao.repository;

import org.apache.ibatis.session.SqlSession;
import ru.lapkisoft.topvishka.dao.mapper.TeacherMapper;
import ru.lapkisoft.topvishka.model.Teacher;
import ru.lapkisoft.topvishka.service.SessionFactoryService;

import java.util.List;
import java.util.UUID;

public class TeacherDaoImpl implements TeacherDao {
    public void insert(Teacher item) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            TeacherMapper mapper = session.getMapper(TeacherMapper.class);
            mapper.insert(item);
            session.commit();
        }
    }

    public Teacher findById(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            TeacherMapper mapper = session.getMapper(TeacherMapper.class);
            return mapper.findById(id);
        }
    }

    @Override
    public List<Teacher> getAll() {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            TeacherMapper mapper = session.getMapper(TeacherMapper.class);
            return mapper.getAll();
        }
    }

    public void delete(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            TeacherMapper mapper = session.getMapper(TeacherMapper.class);
            mapper.delete(id);
            session.commit();
        }
    }

    @Override
    public void updateHirshIndex(UUID id, Integer index) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            TeacherMapper mapper = session.getMapper(TeacherMapper.class);
            mapper.updateHirshIndex(id, index);
            session.commit();
        }
    }

    @Override
    public List<Teacher> getTeachersByEdProgramId(UUID id) {
        try (SqlSession session = SessionFactoryService.getSqlSessionFactory().openSession()) {
            TeacherMapper mapper = session.getMapper(TeacherMapper.class);
            return mapper.getTeachersByEdProgramId(id);
        }
    }
}
