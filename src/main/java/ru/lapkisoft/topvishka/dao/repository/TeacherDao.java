package ru.lapkisoft.topvishka.dao.repository;

import org.springframework.stereotype.Repository;
import ru.lapkisoft.topvishka.model.Teacher;

import java.util.List;
import java.util.UUID;

@Repository
public interface TeacherDao {
    void insert(Teacher item);
    Teacher findById(UUID id);
    List<Teacher> getAll();
    void delete(UUID id);
    void updateHirshIndex(UUID id, Integer index);
    List<Teacher> getTeachersByEdProgramId(UUID id);
}
