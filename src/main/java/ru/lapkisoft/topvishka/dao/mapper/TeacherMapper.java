package ru.lapkisoft.topvishka.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.lapkisoft.topvishka.model.Teacher;

import java.util.List;
import java.util.UUID;

@Mapper
public interface TeacherMapper extends CrudMapper<Teacher> {
    void updateHirshIndex(@Param("id") UUID id, @Param("index") Integer index);
    List<Teacher> getTeachersByEdProgramId(UUID id);
}
