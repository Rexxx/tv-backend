package ru.lapkisoft.topvishka.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.lapkisoft.topvishka.model.EducationalProgram;
import ru.lapkisoft.topvishka.model.EducationalProgramExternalInfo;

import java.util.UUID;

@Mapper
public interface EducationalProgramMapper extends CrudMapper<EducationalProgram> {
    void addVacanciesKeyword(@Param("id") UUID id, @Param("keyword") String keyword);
    void removeVacanciesKeyword(@Param("id") UUID id, @Param("keyword") String keyword);
    void updateExternalInfo(@Param("id") UUID id, @Param("info") EducationalProgramExternalInfo info);
}
