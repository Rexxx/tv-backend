package ru.lapkisoft.topvishka.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.lapkisoft.topvishka.model.Discipline;

import java.util.List;
import java.util.UUID;

@Mapper
public interface DisciplineMapper extends CrudMapper<Discipline> {
    List<Discipline> getAllByEdProgramId(UUID id);
}
