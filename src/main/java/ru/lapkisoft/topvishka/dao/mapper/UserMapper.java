package ru.lapkisoft.topvishka.dao.mapper;

import ru.lapkisoft.topvishka.model.User;

public interface UserMapper extends CrudMapper<User> {
    User findByEmail(String email);
}
