package ru.lapkisoft.topvishka.dao.mapper;

import ru.lapkisoft.topvishka.model.PojoModel;

import java.util.List;
import java.util.UUID;

public interface CrudMapper<Pojo extends PojoModel> {
    void insert(Pojo pojo);
    Pojo findById(UUID id);
    List<Pojo> getAll();
    void delete(UUID id);
}
