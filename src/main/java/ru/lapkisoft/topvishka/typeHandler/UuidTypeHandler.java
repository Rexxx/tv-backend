package ru.lapkisoft.topvishka.typeHandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class UuidTypeHandler extends BaseTypeHandler<UUID> {
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int parameterIndex, UUID uuid,
                                    JdbcType jdbcType) throws SQLException {
        preparedStatement.setObject(parameterIndex, uuid, jdbcType.TYPE_CODE);
    }

    @Override
    public UUID getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
        return (UUID)resultSet.getObject(columnName);
    }

    @Override
    public UUID getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
        return (UUID)resultSet.getObject(columnIndex);
    }

    @Override
    public UUID getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
        return (UUID)callableStatement.getObject(columnIndex);
    }
}
