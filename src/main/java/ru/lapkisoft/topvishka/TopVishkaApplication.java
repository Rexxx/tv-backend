package ru.lapkisoft.topvishka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.lapkisoft.topvishka.dao.repository.DisciplineDao;
import ru.lapkisoft.topvishka.dao.repository.DisciplineDaoImpl;
import ru.lapkisoft.topvishka.dao.repository.EducationalProgramDao;
import ru.lapkisoft.topvishka.dao.repository.EducationalProgramDaoImpl;
import ru.lapkisoft.topvishka.dao.repository.TeacherDao;
import ru.lapkisoft.topvishka.dao.repository.TeacherDaoImpl;

@Configuration
@SpringBootApplication
public class TopVishkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopVishkaApplication.class, args);
	}

	@Bean
	public EducationalProgramDao educationalProgramDao() {
		return new EducationalProgramDaoImpl();
	}

	@Bean
	public DisciplineDao disciplineDao() {
		return new DisciplineDaoImpl();
	}

	@Bean
	public TeacherDao teacherDao() {
		return new TeacherDaoImpl();
	}
}
