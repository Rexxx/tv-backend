CREATE TABLE IF NOT EXISTS teacher (
    id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    name TEXT NOT NULL,
    scopus_link TEXT NOT NULL,
    hirsh_index INTEGER NOT NULL DEFAULT 0,
    created_date TIMESTAMP NOT NULL DEFAULT now(),
    updated_date TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS discipline (
    id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    name TEXT NOT NULL,
    teacher_id uuid NOT NULL,
    ed_program_id uuid NOT NULL,
    created_date TIMESTAMP NOT NULL DEFAULT now(),
    updated_date TIMESTAMP NOT NULL DEFAULT now()
);

CREATE INDEX IF NOT EXISTS idx_discipline_teacher_id ON discipline (teacher_id);
CREATE INDEX IF NOT EXISTS idx_discipline_ed_program_id ON discipline (ed_program_id);

INSERT INTO teacher (id, name, scopus_link)
    VALUES
    ('9919bffb-5cbc-4bb1-ad00-237ea3269649', 'Горбатов С. В.', 'https://www.scopus.com/authid/detail.uri?authorId=57210738423'),
    ('c2dbb4e8-a4a9-4f95-92a7-51749e6069fa', 'Тихонов Ю. А.', 'https://www.scopus.com/authid/detail.uri?authorId=7003575824'),
    ('4c6dfb06-b534-4abb-b339-d0250d4e6158', 'Чистяков А. Е.', 'https://www.scopus.com/authid/detail.uri?authorId=57203921718'),
    ('2dc01952-1cf7-4574-927d-ce8e7176997c', 'Шерстобитов А. А.', 'https://www.scopus.com/authid/detail.uri?authorId=56735405900'),
    ('21c86e9c-6db5-431e-ab6d-a7dbfa6a6eb0', 'Малягин С. В.', 'https://www.scopus.com/authid/detail.uri?authorId=57193264433'),
    ('36a4115f-7041-4e34-8659-ac78c2966c26', 'Иванов А. В.', 'https://www.scopus.com/authid/detail.uri?authorId=35588854100');

INSERT INTO discipline (id, name, teacher_id, ed_program_id)
    VALUES
    ('077f631d-be0b-4cfa-aad3-21b10a78f690', 'Физика', 'c2dbb4e8-a4a9-4f95-92a7-51749e6069fa', '7bfcb26d-4c29-4a0a-a190-a291822a3c7b'),
    ('e1854071-bd77-4de5-b347-5476db74609e', 'Культура устной и письменной речи', '9919bffb-5cbc-4bb1-ad00-237ea3269649', 'd63d4497-cbe6-45f2-b91e-e3a50d5a969a'),
    ('a930f714-5fab-4ca0-9a6a-e0c8df5c0f4f', 'Иностранный язык', '4c6dfb06-b534-4abb-b339-d0250d4e6158', 'd63d4497-cbe6-45f2-b91e-e3a50d5a969a'),
    ('33c0b7ef-5b3f-42e6-84d9-466a3b112b39', 'Математика', '2dc01952-1cf7-4574-927d-ce8e7176997c', '7bfcb26d-4c29-4a0a-a190-a291822a3c7b'),
    ('f1311957-53e3-4f14-bc90-7c239af9c86f', 'Теория баз данных', '21c86e9c-6db5-431e-ab6d-a7dbfa6a6eb0', '029c8d08-f4dc-4743-9582-b075e3e7299e'),
    ('ea81b901-fa85-46a2-8fbc-a3f8b3028c11', 'Философия', '36a4115f-7041-4e34-8659-ac78c2966c26', '029c8d08-f4dc-4743-9582-b075e3e7299e');
