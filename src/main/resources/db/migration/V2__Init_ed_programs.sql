CREATE TABLE IF NOT EXISTS ed_program (
    id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    syllabus_link TEXT NOT NULL,
    rate REAL NOT NULL DEFAULT 0,
    vacancies_keywords VARCHAR[] NOT NULL DEFAULT ARRAY[]::VARCHAR[],
    vacancies_count_in_city INTEGER NOT NULL DEFAULT 0,
    vacancies_count_in_country INTEGER NOT NULL DEFAULT 0,
    average_wage REAL NOT NULL DEFAULT 0,
    average_hirsh_index INTEGER NOT NULL DEFAULT 0,
    wage_ratio REAL NOT NULL DEFAULT 0,
    created_date TIMESTAMP NOT NULL DEFAULT now(),
    updated_date TIMESTAMP NOT NULL DEFAULT now()
);

CREATE INDEX IF NOT EXISTS idx_ed_program_name ON usr (name);

INSERT INTO ed_program (id, name, description, syllabus_link, vacancies_keywords)
    VALUES
    ('029c8d08-f4dc-4743-9582-b075e3e7299e', 'Информационные системы и технологии', 'Базовые знаний о современных технологиях обработки информации, основанных на создании, реализации и эксплуатации автоматизированных информационных систем',
    'https://edu.donstu.ru/Plans/Plan.aspx?id=40146', '{"оператор баз данных", "разработчик", "системные администратор"}'),
    ('d63d4497-cbe6-45f2-b91e-e3a50d5a969a', 'Лингвистика', 'Углубленное изучение лингвистической теории и ее применение в различных прикладных областях современного языкознания, прежде всего в сфере информационных технологий',
    'https://edu.donstu.ru/Plans/Plan.aspx?id=39265', '{"переводчик", "корректор", "лингвист"}'),
    ('7bfcb26d-4c29-4a0a-a190-a291822a3c7b', 'Прикладная механика', 'Изучение теоретической механики, сопротивления материалов, теории механизмов и машин и гидравлики',
    'https://edu.donstu.ru/Plans/Plan.aspx?id=38604', '{"инженер", "техник"}');
