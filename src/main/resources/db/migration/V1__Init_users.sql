CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS usr (
    id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    name TEXT NOT NULL,
    email TEXT NOT NULL,
    role TEXT NOT NULL,
    password TEXT NOT NULL,
    created_date TIMESTAMP NOT NULL DEFAULT now(),
    updated_date TIMESTAMP NOT NULL DEFAULT now()
);

CREATE INDEX IF NOT EXISTS idx_usr_name ON usr (name);
CREATE INDEX IF NOT EXISTS idx_usr_email ON usr (email);

INSERT INTO usr (name, email, role, password) VALUES
    ('Гринев Александр Сергеевич', 'grinev95@bk.ru', 'ADMIN', '$2a$10$S0lMDhSPAlVeEZff1H11een5oxILsKrZNd2.eQjfBQjtb4ZeDono2');
