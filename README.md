# ТоpEdu (Топ Вышка) #

Бэкенд решения кейса ДГТУ с хакатона №2 от ЦП 2020.

### Для запуска нужно:
1. Установить PostgreSQL (версии 12.2 и выше) + не забыть пакет с расширениями postgresql-contrib
2. Создать базу данных `top_vishka` с пользователем `postgres` и паролем `zxcASDewq`
3. Установить Maven (версии 3.3.9 и выше)
4. Собрать проект командой `mvn clean install`
5. Запустить полученный jar командой `java -jar top-vishka-0.0.1-SNAPSHOT.jar`
6. Если все шаги были выполнены верно, приложение запустится, а Flyway создаст все таблицы и прочие сущности с тестовыми данными

### Примеры запросов
##### Образовательные программы
- Создание ОП: `POST`  `http://localhost:8080/api/ed-programs` `{"name": "Прикладная математика", "description": "Lorem ipsum dolor", "syllabusLink": "https://edu.donstu.ru/Plans/Plan.aspx?id=40119"}`
- Получить ОП по ID: `GET`  `http://localhost:8080/api/ed-programs/029c8d08-f4dc-4743-9582-b075e3e7299e` `{}`
- Получить все ОП: `GET`  `http://localhost:8080/api/ed-programs` `{}`
- Удалить ОП: `DELETE`  `http://localhost:8080/api/ed-programs/029c8d08-f4dc-4743-9582-b075e3e7299e` `{}`
- Добавить ключевое слово к ОП (для поиска по HH): `POST`  `http://localhost:8080/api/ed-programs/029c8d08-f4dc-4743-9582-b075e3e7299e/add_keyword?keyword=математик` `{}`
- Удалить ключевое слово к ОП (для поиска по HH): `POST`  `http://localhost:8080/api/ed-programs/029c8d08-f4dc-4743-9582-b075e3e7299e/remove_keyword?keyword=математик` `{}`
- Собрать стороннюю информацию по ОП: `POST`  `http://localhost:8080/api/ed-programs/029c8d08-f4dc-4743-9582-b075e3e7299e/collect_info` `{}`
- Парсим список ОП (vikon): `GET`  `http://localhost:8080/api/ed-programs/vikon/parse?url=donstu.ru` `{}`

##### Преподаватели
- Создание преподавателя: `POST`  `http://localhost:8080/api/teachers` `{"name": "Гринев А. Ю.", "scopusLink": "https://www.scopus.com/authid/detail.uri?authorId=7006802217"}`
- Получить преподавателя по ID: `GET`  `http://localhost:8080/api/teachers/c2dbb4e8-a4a9-4f95-92a7-51749e6069fa` `{}`
- Получить всех преподавателей: `GET`  `http://localhost:8080/api/teachers` `{}`
- Удалить преподавателя: `DELETE`  `http://localhost:8080/api/teachers/c2dbb4e8-a4a9-4f95-92a7-51749e6069fa` `{}`
- Парсим список преподавателей (vikon): `GET`  `http://localhost:8080/api/teachers/vikon/parse?url=sgugit.ru` `{}`

##### Дисциплины
- Создание дисциплину: `POST`  `http://localhost:8080/api/disciplines` `{"name": "Химия", "teacherId": "21c86e9c-6db5-431e-ab6d-a7dbfa6a6eb0", "edProgramId": "029c8d08-f4dc-4743-9582-b075e3e7299e"}`
- Получить дисциплину по ID: `GET`  `http://localhost:8080/api/disciplines/077f631d-be0b-4cfa-aad3-21b10a78f690` `{}`
- Получить все дисциплины: `GET`  `http://localhost:8080/api/disciplines` `{}`
- Удалить дисциплину: `DELETE`  `http://localhost:8080/api/disciplines/077f631d-be0b-4cfa-aad3-21b10a78f690` `{}`
- Получить все дисциплины по ID ОП: `GET`  `http://localhost:8080/api/disciplines/by_ed_program/029c8d08-f4dc-4743-9582-b075e3e7299e` `{}`

P.S. Если не получилось запустить проект, можно обращаться к боевому серверу на `lapkisot.ru` (вместо localhost)
